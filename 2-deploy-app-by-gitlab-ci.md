## 執行步驟
1. Fork [app-nginx](https://gitlab.com/dso-workshop-infra-hardening/app-nginx) 至您的 GitLab 倉儲
    ![](images/fork-project.png)
2. 於 K8s 建立 app-nginx 指定部署的命名空間 (namespace)
    ```sh
    $ kubectl create namespace $USER
    ```
3. 使用先前所建立的 GitLab K8s executor 完成自動化部署
4. 在尚未配置 RBAC 前，這個章節的部署將會失敗。相關錯誤訊息如：
    - ![](images/error-without-rbac.png)

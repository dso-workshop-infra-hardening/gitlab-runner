## 執行步驟
1. 加入 GitLab Helm repository
    ```sh
    $ helm repo add gitlab https://charts.gitlab.io
    ```
2. 建立 namespace
    ```sh
    $ kubectl create namespace gitlab-runner-$USER  #由於我們的環境是多人共用，為避免後續資源衝突，因此資源皆加上識別號
    ```
3. 搜尋 GitLab Runner 的 Helm Chart 版本
    ```sh
    $ helm search repo -l gitlab/gitlab-runner
    ```
4. 與您的 GitLab 管理者取得 Runner 的註冊 token (以 group runner 為例)
    - 請於自己的 GitLab 帳戶下建立一個 GitLab 群組，並至 **Settings** > **CI/CD** > **Runners** > 取得 Runner 的 `registration token`
5. 編輯 Values 檔案
    1. 從官網下載 [values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml)，並修改以下的參數值：
       - `gitlabUrl`
           ```yaml
           gitlabUrl: https://gitlab.com
           ```
       - `runnerRegistrationToken`
           ```yaml
           # 拿掉註解，並改為步驟 4 取得的 token
           runnerRegistrationToken: <token>
           ```
       - `runners.config.image`:
           ```yaml
           image = "bitnami/kubectl:1.21.5"
           ```
       - `runners.tags`:
           ```yaml
           tags: "gitlab-runner, general"
           ```
    2. 或是，您可以直接複製 `files/values.yaml`
       > 您僅需修改 `runnerRegistrationToken`（其餘參數已完成設定）：
       ```sh
       $ export RUNNER_REGISTER_TOKEN=<token>

       # Linux:
       $ sed -i 's/<token>/'"$RUNNER_REGISTER_TOKEN"'/g' values.yaml

       # Mac:
       $ sed -i "" 's/<token>/'"$RUNNER_REGISTER_TOKEN"'/g' values.yaml
       ```
       驗證：
       ```sh
       $ cat values.yaml| grep runnerRegistrationToken
       runnerRegistrationToken: "yourtokenvalue"
       ```
6. 安裝 GitLab runner
    ```sh
    $ helm install --namespace gitlab-runner-$USER gitlab-runner --version 0.34.0 -f values.yaml gitlab/gitlab-runner

    NAME: gitlab-runner
    LAST DEPLOYED: Wed Nov 10 06:43:10 2021
    NAMESPACE: gitlab-runner-player001
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.com"

    Runner namespace "gitlab-runner-player001" was found in runners.config template.
    ```
7. 確認 Runner 已註冊成功 (**Settings** > **CI/CD** > **Runners**)
    ![](images/available-runner.png)